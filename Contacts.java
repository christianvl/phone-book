// package personalPhoneBook;

public class Contacts{

    private String name;
    private String phone;

    public Contacts(){
        this("Undefined", "Undefined");
    }

    public Contacts(String name, String phone){
        setName(name);
        setPhone(phone);
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public String getPhone(){
        return phone;
    }

    public String toString(){
        return "Name:\t" + getName() + "\nPhone:\t" + getPhone();
    }

    public boolean conflict(Contacts contact){
        return this.getName().equals(contact.getName()) ||
            this.getPhone().equals(contact.getPhone());
    }

}
