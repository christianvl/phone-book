// package personalPhoneBook;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class ContactsManager{

    private List<Contacts> contactInfo = new ArrayList<>();
    private static Scanner in = new Scanner(System.in);
    private static final int ADD = 1, REMOVE = 2, SEARCH = 3, EXIT = 0;

    public ContactsManager(){
    }

    public void launch(){
        int choice = -1;
        do {
            choice = menu();
            execute(choice);
        } while (choice != EXIT);
    }

    private int menu(){
    int choice;
        do{
            System.out.println("Enter your selection:" +
                               "\n1) Add new contact" + "\n2) Remove contact" +
                               "\n3) Search contact" + "\n0) Exit");
            boolean hasInt = in.hasNextInt();
            if (hasInt) {
                choice = in.nextInt();
                in.nextLine();
                if (choice >=0 && choice <=3) {
                    break;
                }
                else System.out.println("Invalid choice");
            }
            else {
                System.out.println("Invalid choice");
                in.nextLine();
            }
        } while (true);
        return choice;
    }

    private void execute(int operation){
        switch(operation){
        case ADD:
            addContact(createContact());
            break;
        case REMOVE:
            removeContact(searchContact());
            break;
        case SEARCH:
            displayContact();
            break;
        case EXIT:
            System.out.println("Bye-bye");
            in.close();
        }
    }

    private String getInput(String message){
        String toReturn = "";
        do{
            System.out.println(message);
                toReturn = in.nextLine();
                if (toReturn.isEmpty()) System.out.println("Invalid value");
                else break;
        } while (true);
        return toReturn;
    }

    private Contacts createContact(){
        String name = getInput("Name: ");
        String phone = getInput("Phone: ");
        return new Contacts(name, phone);
    }

    private boolean addContact(Contacts contact){
        return (getContactInfo().add(contact));
    }

    private boolean removeContact(Contacts contact){
        return (getContactInfo().remove(contact));
    }

    private Contacts searchContact(){
        Contacts contact = createContact();
        for (Contacts element: getContactInfo()){
            if (contact.conflict(element)) {
                return getContactInfo().get(getContactInfo().indexOf(element));
            }
        }
        return null;
    }

    private void displayContact(){
        Contacts contact;
        if ((contact = searchContact()) != null) {
            System.out.println(getContactInfo().get(getContactInfo().indexOf(contact))
                               .toString());
        }
        else System.out.println("Contatc not found");
    }

    public List<Contacts> getContactInfo(){
        return contactInfo;
    }

}
